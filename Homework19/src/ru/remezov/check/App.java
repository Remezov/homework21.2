package ru.remezov.check;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App {
    List<Product> products = new ArrayList();
    List<String> strLines = new ArrayList();

    public static void main(String[] args) throws IOException {
        new App();
    }

    public App() throws IOException {
        try (FileInputStream fr = new FileInputStream("products.txt");
             InputStreamReader isr = new InputStreamReader(fr);
             BufferedReader br = new BufferedReader(isr)) {
            String line = null;
            while ((line = br.readLine()) != null) {
                String strLine = line;
                strLines.add(strLine);
            }
            for (int j = 0; j < strLines.size(); j += 3) {
                String name = strLines.get(j);
                double mass = Double.parseDouble(strLines.get(j+1));
                double price = Double.parseDouble(strLines.get(j+2));
                Product product = new Product(name, price, mass);
                products.add(product);
            }
        }
        printCheck();
    }

    public void printCheck() {
        System.out.printf("%-20s%10s%10s%10s\n","Наименование","Цена","Кол-во","Стоимость");

        for (int i=0; i <=50;i++) {
            System.out.print("=");
        }
        System.out.println();

        double sum = 0;
        for (Product product : products) {
            System.out.printf("%-20s%10.2f%10.3f%10.2f\n", product.getName(), product.getPrice(),
                    product.getMass(), (product.getPrice()*product.getMass()));
            sum += product.getPrice()*product.getMass();
        }

        for (int i=0; i <=50;i++) {
            System.out.print("=");
        }
        System.out.println();

        System.out.printf("%-40s%10.2f", "Итого:", sum);
    }
}
