package ru.remezov.reverse;

public class App {
    public static void main(String[] args) {
        int count = 10;
        int[] array = new int[count];

        for (int i = 0; i < count; i++) {
            array[i] = i;
            System.out.print(array[i]+" ");
        }
        System.out.println();

        int var;
        for (int i = 0; i < (array.length/2); i++) {
            var = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = var;
        }

        for (int i = 0; i < count; i++) {
            System.out.print(array[i]+" ");
        }
    }
}
